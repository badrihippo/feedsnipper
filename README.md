# feedsnipper

automatically process and reformat RSS feeds

## Back story

**This script was designed for personal use. It may or may not work for you!**

The current version of this script is designed for the
[Snipette magazine](https://medium.com/snipette). It takes in the feed and
processes it in the following fashion:

  * Truncate the article to at most 6 paragraphs
  * Ensure that the article doesn't go beyond 1 "section" (the end of which is
    detected by the start of a `<figure>` tag, since sections in Snipette are
    seperated by a small image)

The primary reason behind writing this script was to process an RSS feed for the
MailChimp "send email from RSS" feature. The original feed included the whole
article, whereas we wanted only the first few paragraphs to come in the email.
Since MailChimp doesn't allow this fine-grained processing, we made this script
to do it instead.

## Usage

Install dependencies (`pip install requirements.txt`), then run

    python feedsnipper.py

This will read an RSS feed file named `snipette.xml` and print the processed
output to the terminal.

You can also customise the script by importing it into your own, such as in this
example Python script:

    from feedsnipper import *
    fs = FeedSnipper()
    fs.feed_path = 'https://medium.com/feed/snipette'
    xml = fs.render()

    print(xml)

For further customisation, you can extend the class, use `load_regex()` to
change the regular expression, override `process_description` to use its own
regex or change the way it functions, etc. I know that's not a very 
user-friendly way to go about it: if you have time to improve the interface,
feel free to send in a pull-request.