#! /usr/bin/python3.6

# Sample script—you could put this in your /cgi-bin or something.

import traceback
import sys
import re

# sys.path.append('/path/to/feedsnipper')

from feedsnipper import *

print('Content-type: text/plain')
print()

# Customise feedsnipper with a link processor that automatically
# translates certain URLs (eg. if you want to add extra query_string
# options or something

# For Medium, this is  useful if you want to have "friend links" to
# bypass the paywall

class SnipetteFeedSnipper(FeedSnipper):
    def process_link(self, text):
        m = re.match(r'^(?P<path>.*?)\?(?P<query_string>.*)$', text)
        url_path = m.groupdict().get('path')
        text = self.urldict.get(url_path, text)
        print('Returning: %s' % text)
        return(text)

    def process_description(self, text, regex=None):
        if regex is None:
            regex = self.regex
        match = self.regex.match(text)
        if match:
            matches = match.groupdict()
            result = matches['beginning']
        else:
            # Members-only post: use a different regex
            match = re.match(r'^(?P<beginning>.*)\<p class="medium-feed-link"\>\<a href="(?P<url>.*?)\"\>(?P<linktext>.*?)\<\/a\>\<\/p\>\<\/div\>$', text)
            if match:
                matches = match.groupdict()
                result = matches['beginning']
            else:
                result = text
        return result

fs = SnipetteFeedSnipper()

# Load URL dict
# Reads records from urls.txt of URLs to convert
try:
    with open('urls.txt', 'r') as f:
        for line in f:
            record = line.rstrip().lstrip().split()
            if len(record) == 2: fs.urldict[record[0]] = record[1]
except Exception as e:
    print('Error: Failed to load URL dict: %s' %e)

fs.feed_path = 'https://medium.com/feed/snipette'

try:
    xml = fs.render()
    with open('../feed.xml', 'w', encoding='utf-8') as out:
        out.write(xml)
except:
    print(traceback.format_exc())

print('File loaded successfully.')
