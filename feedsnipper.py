#!/usr/bin/python3

import feedparser
import PyRSS2Gen
import re
from datetime import datetime
from time import mktime

class FeedSnipper:
    original_feed = None
    processed_feed = None
    feed_path = 'snipette.xml'
    regex_string = r'^(?P<beginning>.*?\<img .*?src="(?P<image_url>.*?)".*?\<\/figure\>(((?!\<figure\>).)*?\<\/p\>){1,6}).*?\<figure\>'
    regex = None
    urldict = {}

    def load_regex(self, regex_string=None):
        if regex_string is None:
            regex_string = self.regex_string
        self.regex = re.compile(regex_string)

    def process_description(self, text, regex=None):
        if regex is None:
            regex = self.regex
        match = self.regex.match(text)
        if match:
            matches = match.groupdict()
            result = matches['beginning']
        else:
            result = text  # fall back to using original text
        return result

    def process_image(self, text, regex=None):
        if regex is None:
            regex = self.regex
        matches = self.regex.match(text).groupdict()
        result = matches['image_url']
        return result

    def process_link(self, text):
        return text

    def load(self, feed_path=None):
        if feed_path is None:
            feed_path = self.feed_path
        self.original_feed = feedparser.parse(feed_path)

    def process(self):
        if self.original_feed is None:
            self.load()
        if self.regex is None:
            self.load_regex()

        items = [
            PyRSS2Gen.RSSItem(
                title = i.title,
                link = self.process_link(i.link),
                description = self.process_description(i.summary),
                guid = PyRSS2Gen.Guid(i.id),
                categories = [ c.term for c in i.tags],
                author = i.author,
                pubDate = datetime.fromtimestamp(mktime(i.published_parsed)),
                # TODO add image
            )

            for i in self.original_feed.entries]

        rss = PyRSS2Gen.RSS2(
            title = 'Snipette',
            description = self.original_feed.feed.description,
            link = self.original_feed.feed.link,
            language = 'en-GB',
            lastBuildDate = datetime.now(),
            pubDate = datetime.fromtimestamp(mktime(self.original_feed.feed.updated_parsed)),
            image = PyRSS2Gen.Image(
                url = self.original_feed.feed.image.href,
                title = self.original_feed.feed.image.title,
                link = self.original_feed.feed.image.link),
            webMaster = self.original_feed.feed.publisher,

            items = items)
        self.processed_feed = rss

    def render(self, encoding='utf-8'):
        if self.processed_feed is None:
            self.process()

        return self.processed_feed.to_xml(encoding=encoding)

if __name__ == "__main__":
    fs = FeedSnipper()
    xml = fs.render()
    print(xml)
